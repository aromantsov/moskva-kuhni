<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @template name: Страница отзывов
 */

get_header();
?>
	<nav id="site-navigation" class="main-navigation sub_menu" role="navigation">
        <div id="main-menu" class="cat-selector">
            <div class="site-container">
                <?php wp_nav_menu( array( 'theme_location' => 'categor' ) ); ?>
                <div class="clearboth"></div>
            </div>
        </div>
    </nav>


	<div class="site-container ind">
    	<div class="zagh2"><?php the_title();?></div>
    </div>
    
    
    <div class="site-container otzivi_wrapper" id="otzivi">
        <div class="center">
			<?php
            if( have_rows('отзывы_на_странице_отзывов') ):
            while ( have_rows('отзывы_на_странице_отзывов') ) : the_row();
                $txt = get_sub_field('текст_отзыва_на_отзывах');
                $link = get_sub_field('ссылка_на_кухню_на_отзывах');
                $dat = get_sub_field('дата_отзыва_на_отзывах');
                $postid = url_to_postid($link);
                $title = get_the_title($postid);
                echo
                '
                <div class="otz_it otz2">
                    <div class="otz_r"></div>
                    <span class="qt_s"></span>
                    <div class="otz_txt">'.$txt.'
                    </div>
                    <div class="kname">
                        Кухня: <a href="'.$link.'">'.$title.'</a>
                    </div>
                    <div class="otz_d">
                        Дата установки: <span>'.$dat.' г.</span>
                    </div>
                    <span class="qt_e"></span>
                </div>
                ';
            endwhile;
            endif; ?>      	
        </div>
        <div id="sel_butt_21">
        </div>
    </div>
    
    
	<div class="site-container cont_txt">
    	<?php if (have_posts()) : while (have_posts()) : the_post();?>
		<?php the_content(); ?>
        <?php endwhile; endif; ?>
    </div>   
<?php get_sidebar(); ?>
<?php get_footer(); ?>
