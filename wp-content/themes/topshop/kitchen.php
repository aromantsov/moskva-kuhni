<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @template name: Страница кухни
 */

get_header();
//session_start();
// em adaptive
if(!isset($_SESSION))
{
    session_start();
}
// END em adaptive
$is_in = 0;
if (isset($_SESSION['SHOPPING_CART']))
foreach ($_SESSION['SHOPPING_CART'] as $itemNumber => $item)
{
	if($item['ID']==get_the_ID())
	{
		$is_in = 1;
		break;
	}
}
?>
<script>
var kitch_name_z = "<?php if(get_field("название_кухни_в_н1")): echo get_field("название_кухни_в_н1");
else: the_title(); endif;?>";
</script>	
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link href="<?php bloginfo("template_directory");?>/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?php bloginfo("template_directory");?>/owl-carousel/owl.theme.css" rel="stylesheet">
	<script src="<?php bloginfo("template_directory");?>/owl-carousel/owl.carousel.js"></script>

	<div class="site-container">
	<?php 
    
        
        if(function_exists('bcn_display'))
        {
    ?>
        <div class="nx-breadcrumb">
    <?php
            bcn_display();
    ?>
        </div>
    <?php		
        } 
    ?> 
        <div id="kitch_head" class="content-area center">
            <div class="zag kitch">
		<?php if(get_field("название_кухни_в_н1")): ?>
				<h1><?php echo get_field("название_кухни_в_н1");?></h1>
		<?php else: ?>
                <h1>Кухня <?php the_title(); ?></h1>
		<?php endif;?>
            </div>
            <div class="tab_selector some_selector">
            	<ul>
                    <li>Материал фасада: </li>
					<li><a class="act" onclick="select_tab(this,'tab_kitch_0');">Пленка</a></li>
                    <li><a onclick="select_tab(this,'tab_kitch_1');">Пластик</a></li>
					<li><a onclick="select_tab(this,'tab_kitch_2');">Эмаль</a></li>
                    <li><a onclick="select_tab(this,'tab_kitch_3');">Массив дерева</a></li>
			<?php
				/*$it = 0;
				$tabs="";
				if( have_rows('вкладки_материалов_фасада') ):
					while ( have_rows('вкладки_материалов_фасада') ) : the_row();
						echo '<li><a ';
						if($it==0) echo 'class="act" ';
						echo 'onClick="select_tab(this,\'tab_kitch_'.$it.'\');">'.get_sub_field('заголовок_вкладки').'</a></li>';
						$tabs.='
            	<div class="kitch_tab';
						if($it>0) $tabs.=' hide';
						$tabs.='" id="tab_kitch_'.$it.'">
                    <span>Стоимость модели  в базовой компоновке от '.get_sub_field('стоимость_модели_в_базовой_компоновке').' руб.</span>
                    '.get_sub_field('текстовое_наполнение_вкладки').'
                </div>
				';
						$it++;
					endwhile;						
				else :
				endif;*/
			?>
                </ul>
            </div>
            <div class="kitchen_price">
            	<div class="kitch_tab" id="tab_kitch_0">
                    <span><?php if(get_field('стоимость_модели_в_базовой_компоновке1')):?>Стоимость представленной модели за погонный метр от <?php echo get_field('стоимость_модели_в_базовой_компоновке1');?> руб.<?php else:?>Стоимость модели уточняйте у менеджера<?php endif;?></span>
                    <?php echo get_field('текстовое_наполнение_вкладки1'); ?>
                </div>
            	<div class="kitch_tab hide" id="tab_kitch_1">
                    <span><?php if(get_field('стоимость_модели_в_базовой_компоновке2')):?>Стоимость представленной модели за погонный метр от <?php echo get_field('стоимость_модели_в_базовой_компоновке2');?> руб.<?php else:?>Стоимость модели уточняйте у менеджера<?php endif;?></span>
                    <?php echo get_field('текстовое_наполнение_вкладки2'); ?>
                </div>
            	<div class="kitch_tab hide" id="tab_kitch_2">
                    <span><?php if(get_field('стоимость_модели_в_базовой_компоновке3')):?>Стоимость представленной модели за погонный метр от <?php echo get_field('стоимость_модели_в_базовой_компоновке3');?> руб.<?php else:?>Стоимость модели уточняйте у менеджера<?php endif;?></span>
                    <?php echo get_field('текстовое_наполнение_вкладки3'); ?>
                </div>
            	<div class="kitch_tab hide" id="tab_kitch_3">
                    <span><?php if(get_field('стоимость_модели_в_базовой_компоновке4')):?>Стоимость представленной модели за погонный метр от <?php echo get_field('стоимость_модели_в_базовой_компоновке4');?> руб.<?php else:?>Стоимость модели уточняйте у менеджера<?php endif;?></span>
                    <?php echo get_field('текстовое_наполнение_вкладки4'); ?>
                </div>
            </div>
        </div><!-- #primary -->
	</div>
    
	<div class="site-containerft">
        <div id="rg-gallery" class="rg-gallery">
            <div class="rg-image-wrapper">	
                <div class="rg-image"></div>
                <div class="rg-loading"></div>
                <div class="rg-caption-wrapper">
                    <div class="rg-caption" style="display:none;">
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="rg-thumbs">
                <div class="es-carousel-wrapper">
                    <div class="es-nav">
                        <span class="es-nav-prev"></span>
                        <span class="es-nav-next"></span>
                    </div>
                    <div class="es-carousel">
                        <ul>
                        <?php if(get_field('фотогалерея_кухни')): ?>
                        <?php $size = "thumbnail";?>
                        <?php while(the_repeater_field('фотогалерея_кухни')): ?>
                            <?php $image = get_sub_field('изображение_кухни'); ?>
                            <?php $thumb = $image['sizes'][$size] ?>
                                <li><a href="#"><img src="<?php echo $thumb;?>" data-large="<?php echo $image['url'];?>" alt="<?php echo $image['title'];?>" data-description="" /></a></li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="buttons center">
    	<div class="new_butt kitch center" onClick="sh_frm_zak(kitch_name_z);"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAQCAYAAAAmlE46AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMWVkZjUxNC0xN2E4LTM3NGEtYWMwZC02ZGE3YmRkNWVmMGMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Q0FDOUQxQ0M1MUU0MTFFNjkxRTlDMzQ0QzdDMEJBNkIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Q0FDOUQxQ0I1MUU0MTFFNjkxRTlDMzQ0QzdDMEJBNkIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmOTcxNTRhZi01NGE3LWEyNDQtOWU1Zi04NWJkZGU4YjZlYjkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpmYWEyMDc2NS0zY2Y3LTExZTYtOGNiNC1hOTZkOGRhYzM1OWEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7IIH7VAAAA+klEQVR42mKsOJNxlYGBQYuBNHCBBarpDxB/JlITLxAbsEA5rkB8gEiN9iC1TFDOARKceRBEMDGQCWAaI0jQUwYiYH5cDsVEA5jG50D8DIgZgVgXiD8A8SOonCYQswLxJShfAoilYRozgXgjlL0DiDcB8TQofxkQCwGxB5TvC5KH+fEPkitA7P9I/P9o8v+QnVoNxH5Qp1oDsSQokpHi7QIuP4JSzXso+zcQf0Xi/8IXOJOAeCuUrQNlT4fyZYGYH1c8siGJgdjMaGrY0C2D2TgfiHugfpQHYisgLoTKyUE134MGlBBM43VoXCE7hxuIldBcp4jEvgMQYACNhC0L0M9FUAAAAABJRU5ErkJggg==" />
        	Онлайн расчет
    	</div>
    	<div class="new_butt kitch center <?php if($is_in==1): echo 'or" onClick="location.href=\'https://moskva-kuhni.com/sravnenie-kuhon/\';"';else:?>" onClick="cart_add(this,'<?php the_ID();?>');"<?php endif;?>>
        	<?php if($is_in==1):?>Перейти к сравнению<?php else:?><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAQCAYAAADwMZRfAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMWVkZjUxNC0xN2E4LTM3NGEtYWMwZC02ZGE3YmRkNWVmMGMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUE2QjA5MDA1MUU1MTFFNjk0OUY5QjcyNUU0NjJCMkIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUE2QjA4RkY1MUU1MTFFNjk0OUY5QjcyNUU0NjJCMkIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmOTcxNTRhZi01NGE3LWEyNDQtOWU1Zi04NWJkZGU4YjZlYjkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpmYWEyMDc2NS0zY2Y3LTExZTYtOGNiNC1hOTZkOGRhYzM1OWEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4IfJSdAAABZ0lEQVR42pzTzStEURzGccOEGGGiULIhbymFBaWs1FAsB6spkWQnmfKSspKFlSILWVh4XbJDVrNA+QeUZGFhYUHI4Pur59Y13Tvh1qfzMuc8c8495wbiF2MZaZ4ZTGEYB36DMtMEVGAOhVhB7n9CJpGjeiXG/xpSglHVP1TGUfCbkHy0YAkhBQziC6VYQxTtKHcmBdUZQx2qEHCF7mAfuxo3JM7zim0L2fBYpq3gGvNqTyOMeq0gS/32svtsO4uuyW/o17ZacaP+W3TrBXfiRf1WRi1kGXZZkjqNdW3N67HgI+QpoBenzou1iQN4RxlOfO7FHooUEMFZ6unYC5xVvdjn+JMqt3Dud8RhlXf6ty5sYgTZuNLvTe5JwZSQWpXP2ntE7Zi+o3u1m7WAT6+VOCENroBHlXaHOlQPucb+CLF6jav9gAl9iHZDj1P+sNErxJZ2iScsoBqrOrEEetCGQ41LOBO/BRgA/2xCM3KtRBYAAAAASUVORK5CYII=" />Добавить к сравнению<?php endif;?>
    	</div>
    </div>
    
	<?php if (the_content()||get_post_meta(get_the_ID(),'тип_фасада',true)||get_post_meta(get_the_ID(),'цвет_фасада',true)||get_post_meta(get_the_ID(),'фрезеровка_фасадов',true)||get_post_meta(get_the_ID(),'цвет_каркаса',true)||get_post_meta(get_the_ID(),'цвет_столешницы',true)||get_post_meta(get_the_ID(),'витраж_стекло',true)):?>
	<div class="site-container ind">
    	<div class="zagh2">Описание кухни <?php the_title();?></div>
    </div>
	<div class="site-container cont_txt">
    	<?php if (have_posts()) : while (have_posts()) : the_post();?>
		<?php the_content(); ?>
        <?php endwhile; endif; ?>
        <div class="parameters">
        	<?php if(get_post_meta(get_the_ID(),'тип_фасада',true)):?><div><span>Тип фасада:</span> <?php echo get_post_meta(get_the_ID(),'тип_фасада',true);?></div><?php endif;?>
        	<?php if(get_post_meta(get_the_ID(),'цвет_фасада',true)):?><div><span>Цвет фасада:</span> <?php echo get_post_meta(get_the_ID(),'цвет_фасада',true);?></div><?php endif;?>
        	<?php if(get_post_meta(get_the_ID(),'фрезеровка_фасадов',true)):?><div><span>Фрезеровка фасадов:</span> <?php echo get_post_meta(get_the_ID(),'фрезеровка_фасадов',true);?></div><?php endif;?>
        	<?php if(get_post_meta(get_the_ID(),'цвет_каркаса',true)):?><div><span>Цвет каркаса:</span> <?php echo get_post_meta(get_the_ID(),'цвет_каркаса',true);?></div><?php endif;?>
        	<?php if(get_post_meta(get_the_ID(),'цвет_столешницы',true)):?><div><span>Цвет столешницы:</span> <?php echo get_post_meta(get_the_ID(),'цвет_столешницы',true);?></div><?php endif;?>
        	<?php if(get_post_meta(get_the_ID(),'витраж_стекло',true)):?><div><span>Витраж/стекло:</span> <?php echo get_post_meta(get_the_ID(),'витраж_стекло',true);?></div><?php endif;?>
        </div>
    </div>
	<?php endif;?>
    <div id="kitch_catalog">
        <div class="cat_selector">
                    <ul>
        <?php
                    $this_id = get_the_ID();
					$it = 0;
                    $tabs = "";
					$scripts = "";
			if( have_rows('основные_категории_кухонь',2) ):
			while ( have_rows('основные_категории_кухонь',2)) : the_row();
				$pag="";
				$link = get_sub_field('страницы_основных_категорий');
				$postid = url_to_postid($link);
                $thumbs = "";
                echo '
                        <li><a ';
                        if($it==0) echo 'class="act" ';
                        echo 'onClick="select_cat(this,\'cat_main_'.$it.'\')">'.get_the_title($postid).'</a></li>';
                $tabs.='<div class="tabs_on_kitch';
                if($it!=0) $tabs.=' hide';
                $tabs.='" id="cat_main_'.$it.'">';
				$pag = get_field('страницы_в_этой_категории', $postid);
				if($pag)
				{
					for($y = 0; $y<=sizeof($pag); $y++)
					{
						if($pag[$y]['страница_принадлежащая_категории']==get_permalink($this_id)) unset($pag[$y]);
					}
					if(sizeof($pag)>=6)
					{
						shuffle($pag);
						$d = 5;
					}
					else 
					{
						$d = sizeof($pag);
					}
					for($y = 0; $y<=$d; $y++)
					{
						$lin = $pag[$y]['страница_принадлежащая_категории'];
						$posid = url_to_postid($lin);
						$thumbs.="<div class=\"kit_it\" onClick=\"change_kit_it(this,'".$id."')\">".get_the_post_thumbnail($posid, 'thumbnail');
						$thumbs.='<a href="'.$lin.'"><div class="kit_tb_kitch';
						$thumbs.='"><div class="kit_tb_c"><span class="kit_name">Кухня '.get_the_title($posid).'</span>';
						if(get_post_meta($posid,'стоимость_кухни',true)) $thumbs.='<span class="price">от '.get_post_meta($posid,'стоимость_кухни',true).' руб.</span>';
						$thumbs.='</div></div></a>';
						$thumbs.="</div>";
						if($y==2)
						{
							$thumbs.="<div class=\"kit_it this_it\" onClick=\"change_kit_it(this,'".$id."')\">".get_the_post_thumbnail($this_id, 'thumbnail');
							$thumbs.='<div class="kit_tb_kitch';
							$thumbs.='"><div class="kit_tb_c">';
							$thumbs.='</div></div>';
							$thumbs.="</div>";
						}
					}
				}
				else
				{
					$thumbs.="<div class=\"kit_it this_it\" onClick=\"change_kit_it(this,'".$id."')\">".get_the_post_thumbnail($this_id, 'thumbnail');
					$thumbs.='<div class="kit_tb_kitch';
					$thumbs.='"><div class="kit_tb_c">';
					$thumbs.='</div></div>';
					$thumbs.="</div>";
				}
                $tabs.='<div class="kitc_selector" id="kit_selector'.$it.'">'.$thumbs.'</div><div class="kitc_sele_but" id="sel_butt_21'.$it.'">
            </div></div>';
                $it++;
			endwhile;
			endif;
			wp_reset_postdata();
        ?>
                    </ul>
                </div>
        <div class="slider_main">
        	<?php echo $tabs;?>
            <div id="sel_butt_0">
            </div>
        </div>
        <script>
			var elms;
			function select_cat(elm,elm_id)
			{
				var as = document.getElementsByClassName('cat_selector').item(0).getElementsByTagName('a');
				for(var i=0;i<=as.length-1;i++)
				{
					as[i].classList.remove('act');
				}
				elm.classList.add('act')
				<?php 
				for($i=0;$i<=$it-1;$i++)
				{
					echo "
				document.getElementById('cat_main_".$i."').classList.add('hide');";
				}
				?>
				document.getElementById(elm_id).classList.remove('hide');
				elms = document.getElementsByClassName('kitch_img');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.add('hide');
				}
				document.getElementById(elm_id).getElementsByClassName('kitch_img')[0].classList.remove('hide');
				elms = document.getElementsByClassName('kit_tb');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.remove('kit_it_tb');
				}
				document.getElementById(elm_id).getElementsByClassName('kit_tb')[0].classList.add('kit_it_tb');;
			}
			function select_tab(elm,elm_id)
			{
				var as = document.getElementsByClassName('tab_selector').item(0).getElementsByTagName('a');
				for(var i=0;i<=as.length-1;i++)
				{
					as[i].classList.remove('act');
				}
				elm.classList.add('act');
				elms = document.getElementsByClassName('kitch_tab');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.add('hide');
				}
				document.getElementById(elm_id).classList.remove('hide');
			}
			function change_kit_it(elm,elm_id)
			{
				elms = document.getElementsByClassName('kitch_img');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.add('hide');
				}
				document.getElementById(elm_id).classList.remove('hide');
				elms = document.getElementsByClassName('kit_tb');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.remove('kit_it_tb');
				}
				elm.getElementsByClassName('kit_tb')[0].classList.add('kit_it_tb');
			}
			function cart_add(elm,it_id)
			{
				$.ajax({
					type: 'POST',
					url: "<?php bloginfo("template_directory");?>/cart-add.php",
					response: 'text',
					cache: false,
					data: {it_id: it_id},
					success: function(data) 
					{				
						elm.classList.add('or');
						elm.innerHTML = "Перейти к сравнению";
						elm.setAttribute('onclick', 'location.href="https://moskva-kuhni.com/sravnenie-kuhon/"');
						document.getElementById('cart_count').innerHTML="("+data+")";
						document.getElementById('cart_content').classList.remove('hide');
					},
					error: function()
					{
						alert('Произошла ошибка!!!');
					}
				});
			}
		</script>
    </div>
<!--	<div class="site-container rel">-->
    <!-- em adaptive -->
    <div class="site-container rel kitchenAcs">
    	<div class="zagh2 ind">Комплектующие кухни</div>
        <div id="complect">
			<?php
			$inht=0;
            if( have_rows('категории_комплектующих',2) ):
            while ( have_rows('категории_комплектующих',2) ) : the_row();
                $link = get_sub_field('категория_комплектующих');
				$postid = url_to_postid($link);
                echo
                '
            <div class="comp_cat">
			'.get_the_post_thumbnail($postid, 'thumbnail').'
            		<span><a href="'.$link.'">'.get_the_title($postid).'</a></span>
            </div>
                ';
				$inht++;
            endwhile;
            endif; ?>
        </div>
        <?php if($inht>5): ?>
        <div id="sel_butt_2121">
        </div>
        <?php endif; ?>
    </div><!-- END em adaptive -->
        <?php
        if( have_rows('слайдер_последних_фото_кухни') ):?>
    <div id="last_kitchen">
    	<div class="zag">
        	Кухни того же цвета
        </div>
        <div id="last_kuhni">
        <?php
		$it=0;
		while ( have_rows('слайдер_последних_фото_кухни') ) : the_row();
			$image1 = get_sub_field('изображение_1_кухни');
			$image2 = get_sub_field('изображение_2_кухни');
			$image3 = get_sub_field('изображение_3_кухни');
			$image4 = get_sub_field('изображение_4_кухни');
			$image5 = get_sub_field('изображение_5_кухни');
			echo
			'
			<div class="main_sl">
				<div class="img_item_1"><a href="'.$image1['url'].'" rel="lightbox'.$it.'"><img src="'.$image1['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_2"><a href="'.$image2['url'].'" rel="lightbox'.$it.'"><img src="'.$image2['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_3"><a href="'.$image3['url'].'" rel="lightbox'.$it.'"><img src="'.$image3['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_4"><a href="'.$image4['url'].'" rel="lightbox'.$it.'"><img src="'.$image4['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_5"><a href="'.$image5['url'].'" rel="lightbox'.$it.'"><img src="'.$image5['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
			</div>
			';
			$it++;
		endwhile;?>
        </div>
    </div>
	<?php endif; ?>
    
	<?php
        if( have_rows('отзывы_о_кухне') ):?>
    <div id="otzivi">
    	<div class="zag">
        	Отзывы и благодарности
        </div>
        <div class="otz_wr center" id="otz_wr">
        <?php
		while ( have_rows('отзывы_о_кухне') ) : the_row();
			$txt = get_sub_field('текст_отзыва_о_кухне');
			$dat = get_sub_field('дата_отзыва_о_кухне');
			echo
			'
        	<div class="otz_it">
            	<div class="otz_r"></div>
                <span class="qt_s"></span>
                <div class="otz_txt">'.$txt.'
                </div>
                <div class="kname">
                	
                </div>
                <div class="otz_d">
                	Дата установки: <span>'.$dat.' г.</span>
                </div>
                <span class="qt_e"></span>
            </div>
			';
		endwhile; ?>
        </div>
        <div id="sel_butt_21">
        </div>
    </div>
	<?php endif; ?>

    <!-- em adaptive -->
    <div class="new_buttWrapper">
        <div class="new_butt_otz cat center" onclick="sh_frm('us_frm5');">
            Оставьте свой отзыв
        </div>
    </div>
    <!-- END em adaptive -->
<!--    <div class="new_butt_otz cat center" onclick="sh_frm('us_frm5');">-->
<!--        Оставьте свой отзыв-->
<!--    </div>-->
    
    <script>
    jQuery(document).ready(function($) {
      $("#complect").owlCarousel({
    		navigation : true,
			cnt: 2121,
			items: 5,
            itemsDesktop : [1199, 4],
            itemsDesktopSmall : [979, 3],
            itemsTablet : [768, 2],
            itemsTabletSmall : false,
            itemsMobile : [500, 1]
  		});
    });
    jQuery(document).ready(function($) {
      $("#otz_wr").owlCarousel({
    		navigation : true,
			cnt: 21,
			items: 3
  		});
    });
    $("body").data("page", "frontpage");
    </script>
	<script type="text/javascript" src="<?php bloginfo("template_directory");?>/slider/jquery.jshowoff.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_directory");?>/slider/jshowoff.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_directory");?>/kitchen/css/style.css" />
    <script type="text/javascript" src="<?php bloginfo("template_directory");?>/kitchen/js/jquery.tmpl.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo("template_directory");?>/kitchen/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php bloginfo("template_directory");?>/kitchen/js/jquery.elastislide.js"></script>
    <script type="text/javascript" src="<?php bloginfo("template_directory");?>/kitchen/js/gallery.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#last_kuhni").jshowoff
            ({
                speed:600,
                changeSpeed:500,
                effect:"slideLeft",
                controls:2,
                hoverPause:0,
                autoPlay:false
                })
            });
    </script>    
    
    
    
    
    
    
    
    
    
    
    <?php get_sidebar(); ?>
<?php get_footer(); ?>
