<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package topshop
 */
global $woocommerce;
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!-- em adaptive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- END em adaptive -->
<!--<meta name="viewport" content="width=1200">-->
<meta name="yandex-verification" content="47b0c33805c35d8f" />
<meta name="google-site-verification" content="kNZ2rcCdk6_vvIk8AyPmY30nKNW3Nz7RbahRThmB5pA" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<noscript>
			<style>
				.es-carousel ul{
					display:block;
				}
			</style>
		</noscript>
		<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			
		</script>
</head>
<body <?php body_class(); ?>>
<div id="page">
<header id="masthead" class="site-header border-bottom topshop-header-layout-standard" role="banner">
    
    <?php get_template_part( '/templates/header/header-layout-standard' ); ?>
</header><!-- #masthead -->
<!--<div id="content" class="site-content site-container--><?php //echo ( ! is_active_sidebar( 'sidebar-1' ) ) ? ' content-no-sidebar' : ' content-has-sidebar'; ?><!--">-->
    <!-- em adaptive -->
    <div id="content" class="site-content container<?php echo ( ! is_active_sidebar( 'sidebar-1' ) ) ? ' content-no-sidebar' : ' content-has-sidebar'; ?>">
    <!-- END em adaptive -->