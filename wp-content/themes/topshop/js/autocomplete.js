/*
By Evgheni Anghel
site-creating.ru
vk.com/djanghel
*/

( function( $ ) {
    
    jQuery( document ).ready( function() {
        
        $( 'select' ).select2();
		
    });
   
} )( jQuery )