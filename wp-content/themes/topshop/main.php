<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @Template name: Главная страница
 */

get_header(); ?>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo("template_directory");?>/slider/jquery.jshowoff.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_directory");?>/slider/jshowoff.css">
    <div>
    <?php if( function_exists('cyclone_slider') ) cyclone_slider('main-slideshow'); ?>
    </div>
	
	<div id="why_us">
    	<div class="zag">
        	Как получить кухню под ключ
        </div>
        <div class="why_us us1">
 		    Свяжитесь с нами любым способом
        </div>
        <div class="why_us us2">
 		    Составление дизайн проекта
        </div>
        <div class="why_us us3">
	        Наш специалист сделает предварительный расчет
        </div>
        <div class="why_us us4">
        	Замер помещения, оформление договора
        </div>
        <div class="why_us us5">
        	Доставка и сборка
        </div>
    </div>
    
    <div id="main_catalog" onClick="no_auto();">
    	<div class="cat_selector">
        	<ul>
<?php
			$it = 0;
			$tabs = "";
	if( have_rows('основные_категории_кухонь',2) ):
	while ( have_rows('основные_категории_кухонь',2)) : the_row();
		$link = get_sub_field('страницы_основных_категорий');
		$postid = url_to_postid($link);
		$thumbs = "";
		echo '
				<li><a id="kit_sel_a_'.$it.'" ';
				if($it==0) echo 'class="act" ';
				echo 'onClick="select_tab(this,\'tab_main_'.$it.'\')">'.get_the_title($postid).'</a></li>';
		$tabs.='<div class="tabs_on_main';
		if($it!=0) $tabs.=' hide';
		$tabs.='" id="tab_main_'.$it.'">';
		$it_sec=0;
		if( have_rows('страницы_в_этой_категории',$postid) ):
		while ( have_rows('страницы_в_этой_категории',$postid) && $it_sec<5 ) : the_row();
			$lin = get_sub_field('страница_принадлежащая_категории');
			$posid = url_to_postid($lin);
			if(get_page_template_slug($posid)=='kitchen.php'):
			$id="kitch_img_".$it.'_'.$it_sec;
			if( have_rows('фотогалерея_кухни',$posid) ):
			while ( have_rows('фотогалерея_кухни',$posid) ) : the_row();
				$image = get_sub_field('изображение_кухни');
				$tabs.= '
				<div class="kitch_img';
				if($it_sec!=0) $tabs.=" hide";
				$tabs.='" id="'.$id.'">';
				$tabs.='
				<div class="kitch_it_pr">
					<div class="kitch_bord1"></div>
					<div class="kitch_bord2"></div>
					<div class="kitch_price">
						<div class="kitch_it_pr_name">Кухня '.get_the_title($posid).'</div>';
						if(get_post_meta($posid,'стоимость_кухни',true)) $tabs.='<div class="kitch_it_pr_price">Цена: от '.get_post_meta($posid,'стоимость_кухни',true).' руб. за п.м.</div>';
						$tabs.='<a class="new_butt" href="'.$lin.'">
							Все фото кухни
						</a>
					</div>
				</div>';
				$tabs.='<img src="' .$image['sizes']['large']. '"></div>';
				break;						
			endwhile;
			else:
				$tabs.= '
				<div class="kitch_img';
				if($it_sec!=0) $tabs.=" hide";
				$tabs.='" id="'.$id.'">';
				$tabs.='
				<div class="kitch_it_pr">
					<div class="kitch_bord1"></div>
					<div class="kitch_bord2"></div>
					<div class="kitch_price">
						<div class="kitch_it_pr_name">Кухня '.get_the_title($posid).'</div>';
						if(get_post_meta($posid,'стоимость_кухни',true)) $tabs.='<div class="kitch_it_pr_price">Цена: от '.get_post_meta($posid,'стоимость_кухни',true).' руб. за п.м.</div>';
						$tabs.='<a class="new_butt" href="'.$lin.'">
							Все фото кухни
						</a>
					</div>
				</div>';
				$tabs.='<img src="'.get_bloginfo("template_directory").'/no-img.jpg"></div>';
			endif;
			$thumbs.="
			<div class=\"kit_it\" onClick=\"change_kit_it(this,'".$id."')\">
				".get_the_post_thumbnail($posid, 'thumbnail');
			$thumbs.='
				<div class="kit_tb';
			if($it_sec==0) $thumbs.=' kit_it_tb';
			$thumbs.='">
					<div class="kit_tb_c"><span class="kit_name">Кухня '.get_the_title($posid).'</span>';
					if(get_post_meta($posid,'стоимость_кухни',true)) $thumbs.='<span class="price">от '.get_post_meta($posid,'стоимость_кухни',true).' руб.</span>';
			$thumbs.=	'</div>
			</div>';
			$thumbs.="
			</div>";
			$it_sec++;
			endif;
		endwhile;
		endif;
		if(get_the_post_thumbnail($postid, 'thumbnail')) $thumbs.='<div class="kit_it">'.get_the_post_thumbnail($postid, 'thumbnail').'<div class="kit_tbp"><div class="kit_tb_c"><span class="kit_name"><a href="'.$link.'">Все '.get_the_title($postid).'</a></span></div></div></div>';
		$tabs.='
		<div class="kit_selector">'.$thumbs.'</div>
		</div>
		';
		$it++;
		$it_all = $it;
	endwhile;
	endif;
?>
			</ul>
        </div>
        <div class="slider_main">
			<?php
                echo $tabs;
            ?>
        </div>
        <script>
			var elms;
			var noauto = 0;
			function no_auto(){
				noauto=1;
			}
			function select_tab(elm,elm_id)
			{
				var as = document.getElementsByClassName('cat_selector').item(0).getElementsByTagName('a');
				for(var i=0;i<=as.length-1;i++)
				{
					as[i].classList.remove('act');
				}
				elm.classList.add('act');
				<?php 
				for($i=0;$i<=$it-1;$i++)
				{
					echo "
				document.getElementById('tab_main_".$i."').classList.add('hide');";
				}
				?>
				document.getElementById(elm_id).classList.remove('hide');
				elms = document.getElementsByClassName('kitch_img');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.add('hide');
				}
				document.getElementById(elm_id).getElementsByClassName('kitch_img')[0].classList.remove('hide');
				elms = document.getElementsByClassName('kit_tb');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.remove('kit_it_tb');
				}
				document.getElementById(elm_id).getElementsByClassName('kit_tb')[0].classList.add('kit_it_tb');
			}
			function change_kit_it(elm,elm_id)
			{
				elms = document.getElementsByClassName('kitch_img');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.add('hide');
				}
				document.getElementById(elm_id).classList.remove('hide');
				elms = document.getElementsByClassName('kit_tb');
				for(var i=0;i<=elms.length-1;i++)
				{
					elms[i].classList.remove('kit_it_tb');
				}
				elm.getElementsByClassName('kit_tb')[0].classList.add('kit_it_tb');
			}
		</script>
    </div>

    <!-- em adaptive -->
    <div class="new_buttWrapper">
        <a class="new_butt cat center" href="/katalog-kuhon/">
            Перейти в каталог кухонь
        </a>
    </div>
<!-- END em adaptive -->
<!--    <a class="new_butt cat center" href="/katalog-kuhon/">-->
<!--        Перейти в каталог кухонь-->
<!--    </a>-->

	<?php
    if(have_rows('изображение_акции',2)||get_field('текст_акции',2)):?>
	<div id="actions">
    	<div class="zag">
        	акции, спецпредложения и рассрочка
        </div>
        <?php
        if( have_rows('изображение_акции',2) ):
		while ( have_rows('изображение_акции',2) ) : the_row();
			$image = get_sub_field('изобр_акции');
			echo
			'
			<div class="action_img">
				<img src="'.$image['url'].'" />
			</div>
			';
		endwhile;
		endif; ?>
        <div class="act_txt">
       <?php echo get_field('текст_акции',2); ?>
        </div>
    </div>
    <?php endif; ?>
	<div id="last_kitchen">
    	<div class="zag">
        	Лучшие фото кухонь
        </div>
        <div id="last_kuhni">
        <?php
		$it=0;
        if( have_rows('слайдер_последних_фото',2) ):
		while ( have_rows('слайдер_последних_фото',2) ) : the_row();
			$image1 = get_sub_field('изображение_1');
			$image2 = get_sub_field('изображение_2');
			$image3 = get_sub_field('изображение_3');
			$image4 = get_sub_field('изображение_4');
			$image5 = get_sub_field('изображение_5');
			echo
			'
			<div class="main_sl">
				<div class="img_item_1"><a href="'.$image1['url'].'" rel="lightbox'.$it.'"><img src="'.$image1['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_2"><a href="'.$image2['url'].'" rel="lightbox'.$it.'"><img src="'.$image2['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_3"><a href="'.$image3['url'].'" rel="lightbox'.$it.'"><img src="'.$image3['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_4"><a href="'.$image4['url'].'" rel="lightbox'.$it.'"><img src="'.$image4['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
                <div class="img_item_5"><a href="'.$image5['url'].'" rel="lightbox'.$it.'"><img src="'.$image5['url'].'" /></a><i class="fa fa-search-plus" aria-hidden="true"></i></div>
			</div>
			';
			$it++;
		endwhile;
		endif; ?>
        </div>
        <script type="text/javascript">
	$(document).ready(function(){
		$("#last_kuhni").jshowoff
		({
			speed:600,
			changeSpeed:500,
			effect:"slideLeft",
			controls:1,
			hoverPause:0,
			autoPlay:false
			})
		});
</script>
    </div>
    <!-- em adaptive -->
    <div class="new_buttWrapperGallery">
        <a class="new_butt center" href="#">
            Перейти в галерею
        </a>
    </div>
    <!-- END em adaptive -->
<!--    <a class="new_butt center" href="#">-->
<!--    Перейти в галерею-->
<!--    </a>-->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            
            <?php get_template_part( '/templates/titlebar' ); ?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->
    
	<div id="otzivi">
    	<div class="zag">
        	Отзывы и благодарности
        </div>
        <!-- em adaptive -->
        <div class="otzWrapper">
            <div class="otz_wr center" id="otz_wr">
                <?php
                if( have_rows('отзывы_на_главной') ):
                    while ( have_rows('отзывы_на_главной') ) : the_row();
                        $txt = get_sub_field('текст_отзыва_на_главной');
                        $link = get_sub_field('ссылка_на_кухню_на_главной');
                        $dat = get_sub_field('дата_отзыва_на_главной');
                        $postid = url_to_postid($link);
                        $title = get_the_title($postid);
                        echo
                            '
        	<div class="otz_it">
            	<div class="otz_r"></div>
                <span class="qt_s"></span>
                <div class="otz_txt">'.$txt.'
                </div>
                <div class="kname">
                	Кухня: <a href="'.$link.'">'.$title.'</a>
                </div>
                <div class="otz_d">
                	Дата установки: <span>'.$dat.' г.</span>
                </div>
                <span class="qt_e"></span>
            </div>
			';
                    endwhile;
                endif; ?>
            </div>
        </div>
        <!-- END em adaptive -->
        <div id="sel_butt_21">
        </div>
    </div>
    <div id="polez">
    	<div class="zag">
        	Полезная информация
        </div>
        <div class="pol_wr center">
            <div class="pol_item">
                <div class="pol_zag">Несколько полезных советов</div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/poleznye-sovety/kakuyu-kuhnyu-vybrat/" class="a_link">Какую кухню выбрать?</a></div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/poleznye-sovety/kakuyu-stoleshnitsu-vybrat/" class="a_link">Какую столешницу выбрать?</a></div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/poleznye-sovety/kak-vybrat-mojku-dlya-kuhni/" class="a_link">Как выбрать мойку для кухни?</a></div>
                <a href="http://www.moskva-kuhni.com/poleznye-sovety/">Все советы</a>
            </div>
            <div class="pol_item">
                <div class="pol_zag">Популярные вопросы</div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/populyarnye-voprosy/chto-takoe-mdf/" class="a_link">Что такое МДФ?</a></div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/populyarnye-voprosy/furnitura-dlya-kuhni-kakuyu-vybrat/" class="a_link">Фурнитура для кухни. Какую выбрать?</a></div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/populyarnye-voprosy/kak-vybrat-kuhonnyj-garnitur/" class="a_link">Как выбрать кухонный гарнитур?</a></div>
                <a href="https://moskva-kuhni.com/populyarnye-voprosy/">Все вопросы</a>
            </div>
            <div class="pol_item">
                <div class="pol_zag">Новости магазина</div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/aktsii/skidki-dlya-vseh/" class="a_link">Смена экспозиции! Скидка до 70%! Срок действия акции ограничен!</a></div>
                <div class="pol_row"><a href="https://moskva-kuhni.com/aktsii/podarki/" class="a_link">Подарки всем!</a></div>
<div class="pol_row"><a href="https://moskva-kuhni.com/kontakty/" class="a_link">Мы открылись!</a></div>              
                <a href="#">Все новости</a>
            </div>
        </div>
    </div>
    <link href="<?php bloginfo("template_directory");?>/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="<?php bloginfo("template_directory");?>/owl-carousel/owl.theme.css" rel="stylesheet">
	<script src="<?php bloginfo("template_directory");?>/owl-carousel/owl.carousel.js"></script>
    <script>
    jQuery(document).ready(function($) {
    // em adaptive
      $("#otz_wr").owlCarousel({
    		navigation : true,
            cnt: 21,
            items: 3,
            itemsDesktop : [1199, 2],
            itemsDesktopSmall : [979, 1],
            itemsTablet : [768, 1],
            itemsTabletSmall : false,
            itemsMobile : [479, 1]
  		});
    // END em adaptive
    });
    $("body").data("page", "frontpage");
    </script>
<?php get_footer(); ?>
