<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @template name: Страница корзины
 */

get_header(); 
session_start();?>
    <nav id="site-navigation" class="main-navigation sub_menu" role="navigation">
        <div id="main-menu" class="cat-selector">
            <div class="site-container">
                <?php wp_nav_menu( array( 'theme_location' => 'categor' ) ); ?>
                <div class="clearboth"></div>
            </div>
        </div>
    </nav>
    
	<div class="site-container ind">
    	<div class="zagh2">Сравнение кухонь</div>
    </div>
	<?php if($_SESSION['SHOPPING_CART']): ?>
	<div class="site-container cont_txt">
    	<div class="tb" id="prod_tb">
            <div class="tr">
                <div class="th">
                Название
                </div>
                <div class="th">
                Параметры
                </div>
                <div class="th">
                Цена
                </div>
                <div class="th">
                Удалить
                </div>
            </div>
			<?php
				$it = 0;
				if($_SESSION['SHOPPING_CART'])
                foreach ($_SESSION['SHOPPING_CART'] as $itemNumber => $item) {
					echo '
            <div class="tr">
                <div class="td">';
                    echo get_the_post_thumbnail($item['ID'], 'thumbnail').'<a class="kitch_name_array" href="'.get_page_link($item['ID']).'">'.get_the_title($item['ID']).'</a>';
					echo '
                </div>
                <div class="td">
                    <div class="parameters">';
        	if(get_post_meta($item['ID'],'тип_фасада',true)):?><div><span>Тип фасада:</span> <?php echo get_post_meta($item['ID'],'тип_фасада',true);?></div><?php endif;?>
        	<?php if(get_post_meta($item['ID'],'цвет_фасада',true)):?><div><span>Цвет фасада:</span> <?php echo get_post_meta($item['ID'],'цвет_фасада',true);?></div><?php endif;?>
        	<?php if(get_post_meta($item['ID'],'фрезеровка_фасадов',true)):?><div><span>Фрезеровка фасадов:</span> <?php echo get_post_meta($item['ID'],'фрезеровка_фасадов',true);?></div><?php endif;?>
        	<?php if(get_post_meta($item['ID'],'цвет_каркаса',true)):?><div><span>Цвет каркаса:</span> <?php echo get_post_meta($item['ID'],'цвет_каркаса',true);?></div><?php endif;?>
        	<?php if(get_post_meta($item['ID'],'цвет_столешницы',true)):?><div><span>Цвет столешницы:</span> <?php echo get_post_meta($item['ID'],'цвет_столешницы',true);?></div><?php endif;?>
        	<?php if(get_post_meta($item['ID'],'витраж_стекло',true)):?><div><span>Витраж/стекло:</span> <?php echo get_post_meta($item['ID'],'витраж_стекло',true);?></div><?php endif;
					echo '
        </div>
                </div>
                <div class="td">от ';
                    echo '<span>'.get_post_meta($item['ID'],'стоимость_кухни',true).' руб.</span> за погонный метр';
					echo ' 
                </div>
                <div class="td">
					<div class="cart_del" onClick="cart_del(this,\''.$item['ID'].'\')">Удалить</div> 
                </div>
            </div>';
                }
            ?>
            <div class="tr">
                <div class="th">
                	<div class="cart_del" onClick="cart_del_all()">Очистить все</div>
                </div>
                <div class="th">
                </div>
                <div class="th">
                </div>
                <div class="th">
                </div>
            </div>
		</div>
    </div>
    <?php endif; ?>
    <div class="cart_form center" id="cart_form">
    	<div class="cart_form_head">
        Отправить запрос на кухни в сравнении
        </div>
            <input id="cart_nam3" type="text" placeholder="Ваше имя" required>
            <input id="cart_mail3" type="text" placeholder="email или телефон" required>
            <textarea id="cart_comm3" rows="4" placeholder="Комментарий" required></textarea>
        	<button onclick="snd_frm3()" type="submit" class="butt_or center">Отправить запрос</button>
    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>
		function cart_del(elm,it_id)
		{
			$.ajax({
				type: 'POST',
				url: "<?php bloginfo("template_directory");?>/cart-add.php",
				response: 'text',
				cache: false,
				data: {it_del: it_id},
				success: function(data) 
				{				
					elm.parentNode.parentNode.remove();
					if(data<=0)
					{
						document.getElementById('prod_tb').remove();
						document.getElementById('cart_count').innerHTML="(0)";
						document.getElementById('cart_content').classList.add('hide');
					}
					else
					document.getElementById('cart_count').innerHTML="("+data+")";
				},
				error: function()
				{
					alert('Произошла ошибка!!!');
				}
			});
		}
		function cart_del_all(elm,it_id)
		{
			$.ajax({
				type: 'POST',
				url: "<?php bloginfo("template_directory");?>/cart-add.php",
				response: 'text',
				cache: false,
				data: {it_del_all: '1'},
				success: function(data) 
				{
					document.getElementById('prod_tb').remove();
					document.getElementById('cart_count').innerHTML="(0)";
					document.getElementById('cart_content').classList.add('hide');
				},
				error: function()
				{
					alert('Произошла ошибка!!!');
				}
			});
		}
	</script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
