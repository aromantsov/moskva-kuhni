<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package topshop
 */
?>
</div><!-- #content -->

<footer id="colophon"
        class="site-footer"
        role="contentinfo">
<!--	<div class="site-container">-->
    <!-- em adaptive -->
    <div class="container">
        <div class="containerInner">
            <div class="ft_left">
                <img src="<?php bloginfo( "template_directory" ); ?>/images/logo-bt.png"/>
                <div><?php echo get_field( 'адрес_в_футере', 2 ); ?></div>
            </div>
            <div class="ft_ct">
                <?php wp_nav_menu( 'menu=6' ); ?>
            </div>
            <div class="ft_right">
                <div class="searc">
                    <input name="src_text"
                           class="src_txt"
                           placeholder="Поиск по сайту..."/>
                </div>
                <div class="ft_cont">
                    <div class="ft_phone"><?php echo get_field( 'номер_телефона', 2 ); ?></div>
                    <div class="ft_graf">Пн - Пт с 9:00 до 19:00</div>
                </div>
                <div class="ft_back">
                    <div><a href=""
                            class="butt_ora"
                            onClick="sh_frm('us_frm3');">Написать сообщение</a></div>
                    <div>
                        <a href="<?php echo get_field( 'ссылка_на_страницу_в_facebook', 2 ); ?>">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x fafb"></i>
                                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="<?php echo get_field( 'ссылка_на_страницу_в_twitter', 2 ); ?>">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x fatw"></i>
                                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="<?php echo get_field( 'ссылка_на_страницу_в_vkontakte', 2 ); ?>">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x favk"></i>
                                <i class="fa fa-vk fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                        <a href="https://www.instagram.com/moskva_kuhni/?r=nametag">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x fain"></i>
                                <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
	</div><!-- em adaptive -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
	<div class="site-container hidden"
	     style="min-height: inherit; padding: 10px 25px;"><i class="fa fa-diamond"
	                                                         aria-hidden="true"></i> MOSSEO. Техническая поддержка и продвижение сайта
	</div>
</footer> <!-- .site-footer -->
</div>  <!-- #page -->
<div id="stick">
	<a class="srav"
	   href="https://moskva-kuhni.com/sravnenie-kuhon/"
	   title="Перейти к сравнению кухонь"></a>
	<a class="rascc"
	   onClick="sh_frm_zak('Общий заказ рассчета');"
	   title="Заказать рассчет кухни"></a>
	<a class="calll"
	   onclick="sh_frm('us_frm2');"
	   title="заказать обратный звонок"></a>
</div>
<div id="bd"
     onClick="cl_frm()">
</div>
<div class="user_form center"
     id="us_frm2">
	<div class="form_header">
		Заказать обратный звонок<i class="fa fa-times"
		                           aria-hidden="true"
		                           onClick="cl_frm()"></i>
	</div>
	<form>
		Мы обязательно Вам перезвоним
		<div class="f_lab">
			Как к вам обращаться <span>*</span>
		</div>
		<div class="f_inp">
			<input id="us_nam2"
			       type="text"
			       placeholder="Ваше имя"
			       required>
		</div>
		<div class="f_lab">
			Телефон <span>*</span>
		</div>
		<div class="f_inp">
			<input id="us_tel2"
			       type="text"
			       placeholder="+7(999)999-99-99"
			       required>
		</div>
		<button onclick="snd_frm2()"
		        type="submit"
		        class="butt_or center">Отправить
		</button>
	</form>
</div>
<div class="user_form center"
     id="us_frm3">
	<div class="form_header">
		Отправьте нам сообщение<i class="fa fa-times"
		                          aria-hidden="true"
		                          onClick="cl_frm()"></i>
	</div>
	<form>
		<div class="f_lab">
			Как к вам обращаться <span>*</span>
		</div>
		<div class="f_inp">
			<input id="us_nam3"
			       type="text"
			       placeholder="Ваше имя"
			       required>
		</div>
		<div class="f_lab">
			E-mail <span>*</span>
		</div>
		<div class="f_inp">
			<input id="us_mail3"
			       type="text"
			       placeholder="email@email.ru"
			       required>
		</div>
		<div class="f_lab">
			Комментарий <span>*</span>
		</div>
		<div class="f_inp">
			<textarea id="us_comm3"
			          rows="4"
			          placeholder="Комментарий"
			          required></textarea>
		</div>
		<button onclick="snd_frm3()"
		        type="submit"
		        class="butt_or center">Отправить
		</button>
	</form>
</div>
<div class="user_form center"
     id="us_frm5">
	<div class="form_header">
		Оставьте нам отзыв<i class="fa fa-times"
		                     aria-hidden="true"
		                     onClick="cl_frm()"></i>
	</div>
	<form>
		<div class="f_lab">
			Ваше имя <span>*</span>
		</div>
		<div class="f_inp">
			<input id="us_nam3"
			       type="text"
			       placeholder="Ваше имя"
			       required>
		</div>
		<div class="f_lab">
			E-mail <span>*</span>
		</div>
		<div class="f_inp">
			<input id="us_mail3"
			       type="text"
			       placeholder="email@email.ru"
			       required>
		</div>
		<div class="f_lab">
			Текст отзыва <span>*</span>
		</div>
		<div class="f_inp">
			<textarea id="us_comm3"
			          rows="4"
			          placeholder="Комментарий"
			          required></textarea>
		</div>
		<button onclick="snd_frm3()"
		        type="submit"
		        class="butt_or center">Отправить
		</button>
	</form>
</div>
<div class="user_form center"
     id="us_frm4">
	<div class="form_header">
		Расчет стоимости заказа кухни<i class="fa fa-times"
		                                aria-hidden="true"
		                                onClick="cl_frm()"></i>
	</div>
	<div id="us_frm4_mes">
		<form class="frm4">
			<div class="f_lab">
				1. Прикрепить проект
				<input type="file"/>
			</div>
			<div class="f_lab">
				2. Выберите тип кухни: прямая, с левым углом, с правым углом, п-образная
				<img src="<?php bloginfo( "template_directory" ); ?>/images/rascet.jpg"
				     alt="выберите тип кухни"
				     class="center"/>
			</div>
			<div class="f_inp">
				<select id="tip_kuhni">
					<option>Прямая</option>
					<option>С левым углом</option>
					<option>С правым углом</option>
					<option>П-образная</option>
				</select>
			</div>
			<div class="f_lab">
				3. Укажите размеры по сторонам
			</div>
			<div class="f_inp">
				<input class="sizes"
				       id="size-a"
				       type="text"
				       placeholder="по стороне А"
				       required>
				<input class="sizes"
				       id="size-b"
				       type="text"
				       placeholder="по стороне Б"
				       required>
				<input class="sizes"
				       id="size-v"
				       type="text"
				       placeholder="по стороне В"
				       required>
			</div>
			<div class="f_lab">
				4. Укажите перечень встраиваемой бытовой техники (наличие посудомоечной машины, стиральной машины, варки, духовки, микроволновки, х-к, вытяжки)
			</div>
			<div class="f_inp">
				<label><input type="checkbox"
				              class="pereceni"
				              id="posud"/>Посудомоечная машина</label>
				<label><input type="checkbox"
				              class="pereceni"
				              id="stir"/>Стиральная машина</label>
				<label><input type="checkbox"
				              class="pereceni"
				              id="varka"/>Варка</label>
				<label><input type="checkbox"
				              class="pereceni"
				              id="duhovka"/>Духовка</label>
				<label><input type="checkbox"
				              class="pereceni"
				              id="mikro"/>Микроволновая печь</label>
				<label><input type="checkbox"
				              class="pereceni"
				              id="holod"/>Холодильник</label>
				<label><input type="checkbox"
				              class="pereceni"
				              id="viteaj"/>Вытяжка</label>
			</div>
			<div class="f_lab">
				5. Выберите тип фасада, тип столешницы
			</div>
			<div class="f_inp">
				<select id="tip_fasada"
				        class="fs-type">
					<option>Пленка</option>
					<option>Пластик</option>
					<option>Эмаль</option>
					<option>Акрил</option>
					<option>Массив</option>
				</select>
				<select id="tip_stoleshnitsi"
				        class="fs-type">
					<option>Пластик</option>
					<option>Искусственный камень</option>
					<option>Кварц</option>
				</select>
			</div>
			<div class="f_lab">
				6. Укажите особые пожелания
			</div>
			<div class="f_inp">
				<textarea id="us_comm4"
				          rows="4"
				          placeholder="Впишите особые пожелания"
				          required></textarea>
			</div>
			<div class="f_lab">
				7. Укажите Ваше имя и фамилию, а также реквизиты для связи с Вами.
			</div>
			<div class="f_inp">
				<input id="us_name4"
				       type="text"
				       placeholder="ФИО"
				       required>
			</div>
			<div class="f_inp">
				<input id="us_mail4"
				       type="text"
				       placeholder="email@email.ru *"
				       required>
			</div>
			<div class="f_inp">
				<input id="us_tel4"
				       type="text"
				       placeholder="Номер телефона *"
				       required>
			</div>
		</form>
<!--		<button onclick="snd_frm4()"-->
<!--		        type="submit"-->
<!--		        class="butt_or center">Отправить-->
<!--		</button>-->
        <!-- em adaptive -->
        <div class="butt_orWrapper">
            <button onclick="snd_frm4()"
                    type="submit"
                    class="butt_or center">Отправить
            </button>
        </div>
        <!-- END em adaptive -->
	</div>
</div>
<script>
	function sh_frm(frm_id) {
		document.getElementById("bd").style.display = 'block';
		document.getElementById(frm_id).style.display = 'block';
	}
	function sh_frm_zak(frm_id) {
		document.getElementById("bd").style.display = 'block';
		document.getElementById("us_frm4").style.display = 'block';
		kitch_name_z = frm_id;
	}
	function cl_frm() {
		document.getElementById("bd").style.display = 'none';
		document.getElementById('us_frm2').style.display = 'none';
		document.getElementById('us_frm3').style.display = 'none';
		document.getElementById('us_frm4').style.display = 'none';
		document.getElementById('us_frm5').style.display = 'none';
	}
	function snd_frm2() {
		var us_n = document.getElementById('us_nam2').value;
		var us_t = document.getElementById('us_tel2').value;
		$.ajax({
			type: 'POST',
			url: "<?php bloginfo( "template_directory" );?>/zayavka2.php",
			response: 'text',
			cache: false,
			data: {Us_name: us_n, Us_phone: us_t},
			success: function (data) {
				document.getElementById('us_frm2').innerHTML = "Спасибо за заявку. Вскоре, наш менеджер с Вами свяжется!";
			},
			error: function () {
				//alert('Произошла ошибка!!!');
			}
		});
	}
	function snd_frm3() {
		var us_n = document.getElementById('us_nam3').value;
		var us_m = document.getElementById('us_mail3').value;
		var us_c = document.getElementById('us_comm3').value;
		$.ajax({
			type: 'POST',
			url: "<?php bloginfo( "template_directory" );?>/zayavka1.php",
			response: 'text',
			cache: false,
			data: {Us_name: us_n, Us_mail: us_m, Us_comm: us_c},
			success: function (data) {
				document.getElementById('us_frm3').innerHTML = "Спасибо за заявку. Вскоре, наш менеджер с Вами свяжется!";
			},
			error: function () {
				//alert('Произошла ошибка!!!');
			}
		});
	}
	function snd_frm4() {
		var kuh_name = kitch_name_z;
		var selec, val, pereceni = "";
		selec = document.getElementById("tip_kuhni");
		var tip_kuhni = selec.options[selec.selectedIndex].value;
		var sizea = document.getElementById("size-a").value;
		var sizeb = document.getElementById("size-b").value;
		var sizev = document.getElementById("size-v").value;
		if (document.getElementById("posud").checked) pereceni += "Посудомоечная машина,";
		if (document.getElementById("stir").checked) pereceni += "Стиральная машина,";
		if (document.getElementById("varka").checked) pereceni += "Варка,";
		if (document.getElementById("duhovka").checked) pereceni += "Духовка,";
		if (document.getElementById("mikro").checked) pereceni += "Микроволновая печь,";
		if (document.getElementById("holod").checked) pereceni += "Холодильник,";
		if (document.getElementById("viteaj").checked) pereceni += "Вытяжка,";
		selec = document.getElementById("tip_fasada");
		var tip_fasada = selec.options[selec.selectedIndex].value;
		selec = document.getElementById("tip_stoleshnitsi");
		var tip_stoleshnitsi = selec.options[selec.selectedIndex].value;
		var us_comm4 = document.getElementById("us_comm4").value;
		var us_name4 = document.getElementById("us_name4").value;
		var us_mail4 = document.getElementById("us_mail4").value;
		var us_tel4 = document.getElementById("us_tel4").value;
		if(!us_mail4)
		{
			document.getElementById("us_mail4").style.borderColor = '#f00';
			document.getElementById("us_mail4").style.borderWidth = '3px';
		}
		else if(!us_tel4)
		{
			document.getElementById("us_tel4").style.borderColor = '#f00';
			document.getElementById("us_tel4").style.borderWidth = '3px';
		}
		else
		$.ajax({
			type: 'POST',
			url: "<?php bloginfo( "template_directory" );?>/zayavka-rascet.php",
			response: 'text',
			cache: false,
			data: {kuh_name: kuh_name, tip_kuhni: tip_kuhni, sizea: sizea, sizeb: sizeb, sizev: sizev, pereceni: pereceni, tip_fasada: tip_fasada, tip_stoleshnitsi: tip_stoleshnitsi, us_comm: us_comm4, us_name: us_name4, us_mail: us_mail4, us_tel: us_tel4},
			success: function (data) {
				if (data == 1) document.getElementById('us_frm4_mes').innerHTML = "<div id='us_frm4_mess'>Спасибо за заявку. Вскоре, наш менеджер с Вами свяжется!</div>";
				else document.getElementById('us_frm4_mes').innerHTML = data;
			},
			error: function () {
				alert('Произошла ошибка!!!');
			}
		});
	}
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript">jQuery(function (i) {
		i.mask.definitions["~"] = "[+-]", i("#us_tel2").mask("+7(999)999-99-99")
	});</script>
<script type="text/javascript">jQuery(function (i) {
		i.mask.definitions["~"] = "[+-]", i("#us_tel4").mask("+7(999)999-99-99")
	});</script>
<link rel="stylesheet"
      href="<?php bloginfo( "template_directory" ); ?>/js/src/css/lightbox.css"
      type="text/css">
<script type="text/javascript"
        src="<?php bloginfo( "template_directory" ); ?>/js/src/js/lightbox.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function () {
			try {
				w.yaCounter39736400 = new Ya.Metrika({
					id: 39736400,
					clickmap: true,
					trackLinks: true,
					accurateTrackBounce: true,
					webvisor: true,
					trackHash: true
				});
			} catch (e) {
			}
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () {
				n.parentNode.insertBefore(s, n);
			};
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else {
			f();
		}
	})(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
	<div><img src="https://mc.yandex.ru/watch/39736400"
	          style="position:absolute; left:-9999px;"
	          alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<?php wp_footer(); ?>
<script>
	lightbox.option({
		'resizeDuration': 500,
		'wrapAround': true,
		'disableScrolling': true,
		'albumLabel': '%1-е фото кухни из %2',
	})
</script>
<!-- em adaptive -->
<script src="wp-content/themes/topshop/js/topshop-adaptive.js"></script>
<!-- END em adaptive -->
</body>
</html>