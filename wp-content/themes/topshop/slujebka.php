<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @template name: Служебная страница
 */

get_header();
?>
    <nav id="site-navigation" class="main-navigation sub_menu" role="navigation">
        <div id="main-menu" class="cat-selector">
            <div class="site-container">
                <?php wp_nav_menu( array( 'theme_location' => 'categor' ) ); ?>
                <div class="clearboth"></div>
            </div>
        </div>
    </nav>
    	<?php 
    
        
        if(function_exists('bcn_display'))
        {
    ?>
        <div class="nx-breadcrumb">
    <?php
            bcn_display();
    ?>
        </div>
    <?php		
        } 
    ?> 

	<div class="site-container ind">
        <?php if(get_field("заголовок_служебной_страницы_h1")): ?>
				<div class="zagh2"><h1><?php echo get_field("заголовок_служебной_страницы_h1");?></h1></div>
		<?php else: ?>
                <div class="zagh2"><h1><?php the_title(); ?></h1></div>
		<?php endif;?>
    </div>
	<div class="site-container cont_txt">
    	<?php if (have_posts()) : while (have_posts()) : the_post();?>
		<?php the_content(); ?>
        <?php endwhile; endif; ?>
    </div>      
<?php get_sidebar(); ?>
<?php get_footer(); ?>
