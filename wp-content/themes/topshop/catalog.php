<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @Template name: Страница каталога
 */

get_header(); ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<nav id="site-navigation" class="main-navigation sub_menu" role="navigation">
        <div id="main-menu" class="cat-selector">
            <div class="site-container">
                <?php wp_nav_menu( array( 'theme_location' => 'categor' ) ); ?>
                <div class="clearboth"></div>
            </div>
        </div>
    </nav>
    	<?php 
    
        
        if(function_exists('bcn_display'))
        {
    ?>
        <div class="nx-breadcrumb">
    <?php
            bcn_display();
    ?>
        </div>
    <?php		
        } 
    ?> 
    
	<div id="otzivi">
    	<div class="zag">
        	<h1><?php the_title();?></h1>
        </div>
        <div class="catalog_wr center">
        <?php $args = array(
                'sort_order'   => 'ASC',
                'sort_column'  => 'post_title',
                'hierarchical' => 1,
                'parent'       => get_the_ID(),
                'post_type'    => 'page',
                'post_status'  => 'publish',
            ); 
            $pages = get_pages($args);
			if( have_rows('страницы_в_этой_категории',get_the_ID()) ):
			while ( have_rows('страницы_в_этой_категории',get_the_ID())) : the_row();
			$lin = get_sub_field('страница_принадлежащая_категории');
			$posid = url_to_postid($lin);
            ?>
        	<div class="cat_it">
            	<div class="otz_r"></div>
                <div class="catalog_txt"><a href="<?php echo get_page_link($posid);?>"><?php echo get_the_post_thumbnail($posid, 'medium'); ?></a></div>
                <div class="cta_label">
                	<a href="<?php echo get_page_link($posid);?>"><?php echo get_the_title($posid);?><br />
                    <?php if(get_post_meta($posid,'стоимость_кухни',true)) echo 'от '.get_post_meta($posid,'стоимость_кухни',true).' руб.';?></a>
                    <div class="new_buttt kitch center" onClick="sh_frm_zak('<?php echo get_the_title($posid);?>');">Заказать расчет</div>
                </div>
            </div>
			<?php
            endwhile;
			endif;
			?>
        </div>
    </div>
    <div class="site-container cont_txt">
    	<?php if (have_posts()) : while (have_posts()) : the_post();?>
		<?php the_content(); ?>
        <?php endwhile; endif; ?>
    </div>
<?php get_footer(); ?>
