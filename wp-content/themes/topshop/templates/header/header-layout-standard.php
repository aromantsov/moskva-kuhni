<?php global $woocommerce; ?>

<?php if( get_theme_mod( 'topshop-show-header-top-bar', false ) ) : ?>
    
    <div class="site-top-bar border-bottom">
<!--        <div class="site-container">-->
        <!-- em adaptive -->
        <div class="container">
            
            <div class="site-top-bar-left">
                
                <?php wp_nav_menu( array( 'theme_location' => 'top-bar', 'fallback_cb' => false, 'depth'  => 1 ) ); ?>
                
            </div>
            <div class="site-top-bar-right">
                
                <?php if ( topshop_is_woocommerce_activated() ) : ?>
                    <div class="site-top-bar-left-text"><?php echo wp_kses_post( get_theme_mod( 'topshop-header-info-text', 'Call Us: 082 444 BOOM' ) ) ?></div>
                <?php endif; ?>
                
                <?php if( get_theme_mod( 'topshop-header-search', false ) ) : ?>
                    <i class="fa fa-search search-btn"></i>
                <?php endif; ?>
                
            </div>
            <div class="clearboth"></div>
            
            <?php if( get_theme_mod( 'topshop-header-search', false ) ) : ?>
                <div class="search-block">
                    <?php get_search_form(); ?>
                </div>
            <?php endif; ?>
            
        </div><!-- END em adaptive -->
        
    </div>

<?php endif; ?>

<!--<div class="site-container">-->
<!-- em adaptive -->
<div class="container">

    <div class="site-header-left">
        <div class="top_logo">
			<?php if( get_header_image() ) : ?>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php esc_url( header_image() ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ) ?>" /></a>
            <?php else : ?>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
                <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
            <?php endif; ?>
        </div>
        <div class="top_zag">
        Лучшие кухни на заказ в Москве
        </div>
        <div id="cart_content" class="<?php
	session_start();
	if(!isset($_SESSION['SHOPPING_CART'])||count($_SESSION['SHOPPING_CART'])<=0):?>hide <?php endif;?>"><a href="/kuhni.ru/korzina-zhelanij/"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAQCAYAAADwMZRfAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NTc3MiwgMjAxNC8wMS8xMy0xOTo0NDowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMWVkZjUxNC0xN2E4LTM3NGEtYWMwZC02ZGE3YmRkNWVmMGMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUE2QjA5MDA1MUU1MTFFNjk0OUY5QjcyNUU0NjJCMkIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUE2QjA4RkY1MUU1MTFFNjk0OUY5QjcyNUU0NjJCMkIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpmOTcxNTRhZi01NGE3LWEyNDQtOWU1Zi04NWJkZGU4YjZlYjkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpmYWEyMDc2NS0zY2Y3LTExZTYtOGNiNC1hOTZkOGRhYzM1OWEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4IfJSdAAABZ0lEQVR42pzTzStEURzGccOEGGGiULIhbymFBaWs1FAsB6spkWQnmfKSspKFlSILWVh4XbJDVrNA+QeUZGFhYUHI4Pur59Y13Tvh1qfzMuc8c8495wbiF2MZaZ4ZTGEYB36DMtMEVGAOhVhB7n9CJpGjeiXG/xpSglHVP1TGUfCbkHy0YAkhBQziC6VYQxTtKHcmBdUZQx2qEHCF7mAfuxo3JM7zim0L2fBYpq3gGvNqTyOMeq0gS/32svtsO4uuyW/o17ZacaP+W3TrBXfiRf1WRi1kGXZZkjqNdW3N67HgI+QpoBenzou1iQN4RxlOfO7FHooUEMFZ6unYC5xVvdjn+JMqt3Dud8RhlXf6ty5sYgTZuNLvTe5JwZSQWpXP2ntE7Zi+o3u1m7WAT6+VOCENroBHlXaHOlQPucb+CLF6jav9gAl9iHZDj1P+sNErxJZ2iScsoBqrOrEEetCGQ41LOBO/BRgA/2xCM3KtRBYAAAAASUVORK5CYII=" />Сравнить кухни
        	<span id="cart_count">
            (<?php if(!isset($_SESSION['SHOPPING_CART'])||count($_SESSION['SHOPPING_CART'])>0): echo count($_SESSION['SHOPPING_CART']); else: echo "0";endif;
	?>)
            </span></a>
        </div>
    </div><!-- .site-branding -->
    
    <div class="site-header-right">
    	<div class="shrl">
        	<div class="ft_phone"><?php echo get_field('номер_телефона',2); ?></div>
            <div class="top_back" onClick="sh_frm('us_frm2');">Обратный звонок</div>
            <div class="graf"><?php echo get_field('график_работы',2); ?></div>
            <!-- <div class="accounts"><i class="fa fa-skype" aria-hidden="true"></i><?php echo get_field('skype',2); ?></div>
            <div class="accounts"><i class="fa fa-certificate" aria-hidden="true"></i><?php echo get_field('icq',2); ?></div> -->
            <div class="accounts"><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo get_field('e-mail',2); ?></div>
        </div>
    	<div class="shrr">
        	<a href="<?php echo get_field('ссылка_на_страницу_в_facebook',2); ?>" target="_blank">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x fafb"></i>
                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
            </a>
            <a href="<?php echo get_field('ссылка_на_страницу_в_twitter',2); ?>" target="_blank">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x fatw"></i>
                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
            </a>
            <a href="<?php echo get_field('ссылка_на_страницу_в_vkontakte',2); ?>" target="_blank">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x favk"></i>
                    <i class="fa fa-vk fa-stack-1x fa-inverse"></i>
                </span>
            </a>
            <a href="https://www.instagram.com/moskva_kuhni/?r=nametag" target="_blank">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x fain"></i>
                    <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="clearboth"></div>
    
</div><!-- END em adaptive -->

<nav id="site-navigation" class="main-navigation <?php echo ( get_theme_mod( 'topshop-sticky-header', false ) ) ? ' header-stick' : ''; ?>" role="navigation">
    <div id="main-menu" class="main-menu-container">
<!--        <span class="main-menu-close"><i class="fa fa-angle-right"></i><i class="fa fa-angle-left"></i></span>-->
<!--        <div class="site-container">-->
<!--            --><?php //wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
<!--            <div class="clearboth"></div>-->
<!--        </div>-->
        <div class="container">
            <div class="menu-wrapper">
                <div class="navbar-header">
                    <button type="button"
                            class="navbar-toggle collapsed"
                            data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1"
                            aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="nav navbar-nav">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
                    </div>
                </div>
                <div class="clearboth"></div>
            </div>
        </div>
    </div>
</nav><!-- #site-navigation -->